package com.my.springboot;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import lombok.extern.log4j.Log4j;

@Configuration
@Log4j
public class AppInterceptor  extends WebMvcConfigurerAdapter{
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new ApiInterceptor()).addPathPatterns("/**");
    }
    
    public class ApiInterceptor implements HandlerInterceptor {

    	@Override
    	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
    		// TODO Auto-generated method stub
    		log.info("--- Pre Hanlde ---");
    		//validation incoming
    		
    		Boolean isPass = true;
    		return isPass;
    	}
    	
    	@Override
    	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
    			throws Exception {
    		// TODO Auto-generated method stub
    		log.info("--- Post Handle---");
    	}
    	
    	@Override
    	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
    			throws Exception {
    		// TODO Auto-generated method stub
    		log.info("--- After Completion ---");
    		
    	}
    }
}

