package com.my.springboot.service;

import com.my.springboot.model.HelloWorld;

public class HelloWorldService {
	public String showTheWorld(HelloWorld helloWorld) {
		helloWorld.setMagicWord("Alakazam");
		return helloWorld.getMagicWord();
	}
}
