package com.my.springboot.model;

public class HelloWorld {
	private String magicWord;
	
	public String getMagicWord() {
		return magicWord;
	}
	public void setMagicWord(String magicWord) {
		this.magicWord = magicWord;
	}
}